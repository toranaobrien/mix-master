from django.urls import path
from .views import (
    list_spirits,
    delete_spirit  
)


urlpatterns = [
    path("spirits/", list_spirits, name="list_spirits"),
    path("spirits/<int:id>/", delete_spirit, name="delete_spirit"),
]


#     path("salespeople/", list_salespeople, name="list_salespeople"),
#     path(
#         "salespeople/<int:pk>/",
#         delete_salesperson,
#         name="delete_salesperson"
#         ),
#     path("customers/", list_customers, name="list_customers"),
#     path("customers/<int:pk>/", delete_customer, name="delete_customer"),
#     path("sales/", list_sales, name="list_sales"),
#     path("sales/<int:pk>/", delete_sale, name="delete_sale")
# ]