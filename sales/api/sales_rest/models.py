from django.db import models


class Glass(models.Model):
    glass_name = models.CharField(max_length=255, unique=True)
    picture_url = models.URLField()


class Spirit(models.Model):
    spirit_name = models.CharField(max_length=255, unique=True)


class SpiritBrand(models.Model):
    spirit_brand = models.CharField(max_length=255)
    spirit_type = models.ForeignKey(
        Spirit,
        related_name="spirit_type",
        on_delete=models.PROTECT
    )


class Ingredients(models.Model):
    ingredient_name = models.CharField(max_length=255)


class Cocktail(models.Model):
    name = models.CharField(max_length=255)
    spirit = models.ForeignKey(
        Spirit,
        related_name="spirit",
        on_delete=models.PROTECT
    )
    spirit_brand_name = models.ForeignKey(
        SpiritBrand,
        related_name="spirit_brand_name",
        on_delete=models.PROTECT
    )
    glass = models.ForeignKey(
        Glass,
        related_name="glass",
        on_delete=models.PROTECT
    )
    ingredients = models.ForeignKey(
        Ingredients,
        related_name="ingredient",
        on_delete=models.PROTECT
    )
    garnish = models.CharField(max_length=255)
    directions = models.TextField()
    picture_url = models.URLField()






# class AutomobileVO(models.Model):
#     vin = models.CharField(max_length=17, unique=True)
#     sold = models.BooleanField(default=False)


# class Salesperson(models.Model):
#     first_name = models.CharField(max_length=50)
#     last_name = models.CharField(max_length=100)
#     employee_id = models.CharField(max_length=100, unique=True)


# class Customer(models.Model):
#     first_name = models.CharField(max_length=50)
#     last_name = models.CharField(max_length=100)
#     address = models.CharField(max_length=200)
#     phone_number = models.CharField(max_length=12)


# class Sale(models.Model):
#     price = models.IntegerField()
#     salesperson = models.ForeignKey(
#         Salesperson,
#         related_name="sales_person",
#         on_delete=models.PROTECT,
#     )
#     customer = models.ForeignKey(
#         Customer,
#         related_name="customer",
#         on_delete=models.PROTECT,
#     )
#     automobile = models.ForeignKey(
#         AutomobileVO,
#         related_name="automobile",
#         on_delete=models.CASCADE,
#     )
    