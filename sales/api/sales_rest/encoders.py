from common.json import ModelEncoder
from .models import Cocktail, Ingredients, Glass, Spirit


class IngredientsEncoder(ModelEncoder):
    model = Ingredients
    properties = [
        "ingredient_name",
        "id",
    ]


class GlassEncoder(ModelEncoder):
    model = Glass
    properties = [
        "glass_name",
        "id",
        "picture_url",
    ]


class SpiritEncoder(ModelEncoder):
    model = Spirit
    properties = [
        "spirit_name",
        "id",
    ]


class CocktailEncoder(ModelEncoder):
    model = Cocktail
    properties = [
        "glass",
        "spirit",
        "garnish",
        "ingredients",
        "name",
        "picture_url",
        "description",
        "id"
    ]
    encoders = {
        "glass": GlassEncoder(),
        "spirit": SpiritEncoder(),
        "ingredients": IngredientsEncoder(),
    }


