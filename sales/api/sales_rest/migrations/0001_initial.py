# Generated by Django 4.0.3 on 2023-09-15 01:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Glass',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('glass_name', models.CharField(max_length=255, unique=True)),
                ('picture_url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Ingredients',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ingredient_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Spirit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('spirit_name', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='SpiritBrand',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('spirit_brand', models.CharField(max_length=255)),
                ('spirit_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='spirit_type', to='sales_rest.spirit')),
            ],
        ),
        migrations.CreateModel(
            name='Cocktail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('garnish', models.CharField(max_length=255)),
                ('directions', models.TextField()),
                ('picture_url', models.URLField()),
                ('glass', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='glass', to='sales_rest.glass')),
                ('ingredients', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='ingredient', to='sales_rest.ingredients')),
                ('spirit', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='spirit', to='sales_rest.spirit')),
                ('spirit_brand_name', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='spirit_brand_name', to='sales_rest.spiritbrand')),
            ],
        ),
    ]
